-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-09-2019 a las 21:27:33
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `games`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `id_league` int(11) NOT NULL,
  `id_sport` int(11) NOT NULL,
  `teem_1` int(11) NOT NULL,
  `teem_2` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `game`
--

INSERT INTO `game` (`id`, `id_league`, `id_sport`, `teem_1`, `teem_2`, `date`) VALUES
(7, 1, 1, 1, 2, '2019-01-02 10:00:00'),
(8, 2, 1, 3, 4, '2020-01-01 21:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `game_buffer`
--

CREATE TABLE `game_buffer` (
  `id` int(11) NOT NULL,
  `id_league` int(11) NOT NULL,
  `id_sport` int(11) NOT NULL,
  `teem_1` int(11) NOT NULL,
  `teem_2` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `id_source` int(11) NOT NULL,
  `id_game` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `game_buffer`
--

INSERT INTO `game_buffer` (`id`, `id_league`, `id_sport`, `teem_1`, `teem_2`, `date`, `id_source`, `id_game`) VALUES
(17, 1, 1, 1, 2, '2019-01-01 01:01:01', 1, 7),
(18, 1, 1, 1, 2, '2019-01-01 13:00:00', 2, 7),
(19, 1, 1, 1, 2, '2019-01-02 10:00:00', 3, 7),
(20, 1, 1, 1, 2, '2019-01-02 10:00:00', 4, 7),
(21, 2, 1, 3, 4, '2020-01-01 19:00:00', 5, 8),
(22, 2, 1, 3, 4, '2020-01-01 20:00:00', 6, 8),
(23, 2, 1, 3, 4, '2020-01-01 21:00:00', 7, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name_en` varchar(25) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `short_name` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `language`
--

INSERT INTO `language` (`id`, `name_en`, `name_ru`, `short_name`) VALUES
(1, 'russian', 'русский', 'ru'),
(2, 'english', 'английский', 'en');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `league`
--

CREATE TABLE `league` (
  `id` int(11) NOT NULL,
  `id_sport` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `league`
--

INSERT INTO `league` (`id`, `id_sport`, `name_en`, `name_ru`) VALUES
(1, 1, 'UEFA Champions League', 'Лига чемпионов УЕФА'),
(2, 1, 'RPL', 'РПЛ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `source`
--

CREATE TABLE `source` (
  `id` int(11) NOT NULL,
  `url` varchar(555) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `source`
--

INSERT INTO `source` (`id`, `url`) VALUES
(1, 'sportdata.com'),
(2, 'sportdata2.com'),
(3, 'sportdata3.com'),
(4, 'sportdata4.com'),
(5, 'sportdata5.com'),
(6, 'sportdata6.com'),
(7, 'sportdata7.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sport`
--

CREATE TABLE `sport` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sport`
--

INSERT INTO `sport` (`id`, `name_en`, `name_ru`) VALUES
(1, 'football', 'футбол');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teem`
--

CREATE TABLE `teem` (
  `id` int(11) NOT NULL,
  `id_sport` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `teem`
--

INSERT INTO `teem` (`id`, `id_sport`, `name_en`, `name_ru`) VALUES
(1, 1, 'Real Madrid', 'Реал'),
(2, 1, 'Barcelona', 'Барселона'),
(3, 1, 'Spartacus', 'Спартак'),
(4, 1, 'CSKA', 'ЦСКА');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `game_buffer`
--
ALTER TABLE `game_buffer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `league`
--
ALTER TABLE `league`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `source`
--
ALTER TABLE `source`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sport`
--
ALTER TABLE `sport`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `teem`
--
ALTER TABLE `teem`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `game_buffer`
--
ALTER TABLE `game_buffer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `league`
--
ALTER TABLE `league`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `source`
--
ALTER TABLE `source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sport`
--
ALTER TABLE `sport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `teem`
--
ALTER TABLE `teem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
namespace App\Http\Controllers;
use DateTime;
use DB;
use Illuminate\Http\Request;

class return_data extends Controller {
	public function index(Request $request) {
		$game = DB::table('game')->inRandomOrder()->first();
		$q = DB::table('game_buffer')
			->join('league', 'league.id', '=', 'game_buffer.id_league')
			->join('sport', 'sport.id', '=', 'game_buffer.id_sport')
			->join('teem', 'teem.id', '=', 'game_buffer.teem_1')
			->join('teem as teem2', 'teem2.id', '=', 'game_buffer.teem_2')
			->join('source', 'source.id', '=', 'game_buffer.id_source')
			->select('game_buffer.date', 'source.url as source', 'teem.name_ru as teem_1', 'teem2.name_ru as teem_2', 'sport.name_ru as sport', 'league.name_ru as league')
			->where('id_game', $game->id);
		if (!empty($request->input('source'))) {
			$q = $q->where('source.url', $request->input('source'));
		}
		if (!empty($request->input('from')) && !empty($request->input('to'))) {
			$q = $q->whereBetween('date', [new DateTime($request->input('from')), new DateTime($request->input('to'))]);
		}
		$game_data = $q->get();
		return response()->json($game_data);
	}
}

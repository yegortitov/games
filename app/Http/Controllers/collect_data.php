<?php

namespace App\Http\Controllers;
use DateTime;
use DB;
use Illuminate\Http\Request;

class collect_data extends Controller {
	private $data = '[
   {
      "language":"\u0440\u0443\u0441\u0441\u043a\u0438\u0439",
      "sport":"\u0444\u0443\u0442\u0431\u043e\u043b",
      "league":"\u041b\u0438\u0433\u0430 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u043e\u0432 \u0423\u0415\u0424\u0410",
      "teem_1":"\u0420\u0435\u0430\u043b",
      "teem_2":"\u0411\u0430\u0440\u0441\u0435\u043b\u043e\u043d\u0430",
      "start_time":"2019-01-01 01:01:01",
      "source":"sportdata.com"
   },
   {
      "language":"\u0440\u0443\u0441\u0441\u043a\u0438\u0439",
      "sport":"\u0444\u0443\u0442\u0431\u043e\u043b",
      "league":"\u041b\u0438\u0433\u0430 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u043e\u0432 \u0423\u0415\u0424\u0410",
      "teem_1":"\u0420\u0435\u0430\u043b",
      "teem_2":"\u0411\u0430\u0440\u0441\u0435\u043b\u043e\u043d\u0430",
      "start_time":"2019-01-01 13:00:00",
      "source":"sportdata2.com"
   },
   {
      "language":"\u0440\u0443\u0441\u0441\u043a\u0438\u0439",
      "sport":"\u0444\u0443\u0442\u0431\u043e\u043b",
      "league":"\u041b\u0438\u0433\u0430 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u043e\u0432 \u0423\u0415\u0424\u0410",
      "teem_1":"\u0420\u0435\u0430\u043b",
      "teem_2":"\u0411\u0430\u0440\u0441\u0435\u043b\u043e\u043d\u0430",
      "start_time":"2019-01-02 10:00:00",
      "source":"sportdata3.com"
   },
   {
      "language":"\u0440\u0443\u0441\u0441\u043a\u0438\u0439",
      "sport":"\u0444\u0443\u0442\u0431\u043e\u043b",
      "league":"\u041b\u0438\u0433\u0430 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u043e\u0432 \u0423\u0415\u0424\u0410",
      "teem_1":"\u0420\u0435\u0430\u043b",
      "teem_2":"\u0411\u0430\u0440\u0441\u0435\u043b\u043e\u043d\u0430",
      "start_time":"2019-01-02 10:00:00",
      "source":"sportdata4.com"
   },
   {
      "language":"\u0440\u0443\u0441\u0441\u043a\u0438\u0439",
      "sport":"\u0444\u0443\u0442\u0431\u043e\u043b",
      "league":"\u0420\u041f\u041b",
      "teem_1":"\u0421\u043f\u0430\u0440\u0442\u0430\u043a",
      "teem_2":"\u0426\u0421\u041a\u0410",
      "start_time":"2020-01-01 19:00:00",
      "source":"sportdata5.com"
   },
   {
      "language":"\u0440\u0443\u0441\u0441\u043a\u0438\u0439",
      "sport":"\u0444\u0443\u0442\u0431\u043e\u043b",
      "league":"\u0420\u041f\u041b",
      "teem_1":"\u0421\u043f\u0430\u0440\u0442\u0430\u043a",
      "teem_2":"\u0426\u0421\u041a\u0410",
      "start_time":"2020-01-01 20:00:00",
      "source":"sportdata6.com"
   },
   {
      "language":"\u0440\u0443\u0441\u0441\u043a\u0438\u0439",
      "sport":"\u0444\u0443\u0442\u0431\u043e\u043b",
      "league":"\u0420\u041f\u041b",
      "teem_1":"\u0421\u043f\u0430\u0440\u0442\u0430\u043a",
      "teem_2":"\u0426\u0421\u041a\u0410",
      "start_time":"2020-01-01 21:00:00",
      "source":"sportdata7.com"
   }
]';

	public function index(Request $request) {
		//$input = $request->all();exit();
		$input = json_decode($this->data); //Для удобства заменяю статикой
		foreach ($input as $value) {
			$lang = DB::table('language')
				->select('short_name')
				->where('name_ru', '=', $value->language)
				->orWhere('name_en', '=', $value->language)
				->get();
			$item = $this->get_item($value, $lang[0]->short_name);
			if ($this->if_not_exist($item)) {
				$game = $this->get_game($item);
				$this->add_game_buffer($game);
			}

		}

	}
	public function get_item($data, $lang) {
		$item = array();
		$sport = DB::table('sport')
			->select('id')->where('name_' . $lang, '=', $data->sport)->get();
		$league = DB::table('league')
			->select('id')->where('name_' . $lang, '=', $data->league)->get();
		$teem_1 = DB::table('teem')
			->select('id')->where('name_' . $lang, '=', $data->teem_1)->get();
		$teem_2 = DB::table('teem')
			->select('id')->where('name_' . $lang, '=', $data->teem_2)->get();
		$source = DB::table('source')
			->select('id')->where('url', '=', $data->source)->get();

		$item['id_sport'] = $sport[0]->id;
		$item['id_league'] = $league[0]->id;
		$item['teem_1'] = $teem_1[0]->id;
		$item['teem_2'] = $teem_2[0]->id;
		$item['date'] = $data->start_time;
		$item['id_source'] = $source[0]->id;
		return $item;
	}
	public function if_not_exist($item) {
		$game_buffer = DB::table('game_buffer')
			->where([
				['id_league', '=', $item['id_league']],
				['id_sport', '=', $item['id_sport']],
				['teem_1', '=', $item['teem_1']],
				['teem_2', '=', $item['teem_2']],
				['date', '=', $item['date']],
				['id_source', '=', $item['id_source']],
			])
			->get();
		if (count($game_buffer) == 0) {
			return true;
		} else {
			return false;
		}
	}
	public function get_game($item) {
		$d = explode(' ', $item['date']);
		$date = explode('-', $d[0]);
		$time = explode(':', $d[1]);

		$game = DB::table('game')
			->where([
				['id_league', '=', $item['id_league']],
				['id_sport', '=', $item['id_sport']],
				['teem_1', '=', $item['teem_1']],
				['teem_2', '=', $item['teem_2']],
			])
			->whereBetween('date', [date('Y-m-d H:i:s', mktime($time[0] - 26, $time[1], $time[2], $date[1], $date[2], $date[0])), date('Y-m-d H:i:s', mktime($time[0] + 26, $time[1], $time[2], $date[1], $date[2], $date[0]))])
			->get();

		if (count($game) > 0) {
			$item['id_game'] = $game[0]->id;
			if (new DateTime($item['date']) > new DateTime($game[0]->date)) {
				DB::table('game')
					->where('id', $game[0]->id)
					->update(['date' => $item['date']]);
			}
		} else {
			$item['id_game'] = $this->add_game($item);
		}
		return $item;
	}

	public function add_game($item) {
		unset($item['id_source']);
		return DB::table('game')->insertGetId($item);

	}
	public function add_game_buffer($game) {

		DB::table('game_buffer')->insert($game);

	}
}
